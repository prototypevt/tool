package com.xyz.tool.modules.util;

import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Utility class providing methods for processing collection objects. Collections e.g. can be filtered or the elements converted.
 */
public abstract class CollectionUtil {

  private CollectionUtil() {
    // nothing to do
  }

  /**
   * Wandelt eine Menge von Elementen anhand einer Function vom Typ T in den Typ R um.
   *
   * @param collection      umzuwandelnde Collection
   * @param convertFunction Umwandlungsfunktion
   * @param <T>             Typ der Eingabeelemente
   * @param <R>             Typ der Ausgabeelemente
   * @return Liste mit umgewandelten Elementen
   */
  public static <T, R> List<R> convert(Collection<T> collection, Function<T, R> convertFunction) {
    return collection.stream().map(convertFunction).collect(Collectors.toList());
  }

  /**
   * Wandelt eine Menge von Elementen anhand einer Function vom Typ T in den Typ R um. Null Werte werden herausgefiltert.
   *
   * @param collection      umzuwandelnde Collection
   * @param convertFunction Umwandlungsfunktion
   * @param <T>             Typ der Eingabeelemente
   * @param <R>             Typ der Ausgabeelemente
   * @return Liste mit umgewandelten Elementen ohne null Werte
   */
  public static <T, R> List<R> convertIgnoreNull(Collection<T> collection, Function<T, R> convertFunction) {
    return collection.stream().map(convertFunction).filter(Objects::nonNull).collect(Collectors.toList());
  }

  /**
   * Gibt das erste Element zurueck, welches nicht {@code null} ist, sofern existent.
   *
   * @param collection zu durchsuchende Collection
   * @param <R>        Typ des Elements
   * @return den ersten tatsaechlichen Wert zurueck, oder {@code null} wenn keins existiert.
   */
  public static <R> R obtainFirstSecured(Collection<R> collection) {
    return (collection != null) ? findFirstOrNull(collection, Objects::nonNull) : null;
  }

  /**
   * Gibt das erste Element zurueck, auf das die Bedingung zutrifft.
   *
   * @param collection zu durchsuchende Collection
   * @param predicate  Filterfunktion
   * @param <R>        Typ des Elements
   * @return den gefundenen Wert oder null, wenn kein Element auf die Filterfunktion zutrifft.
   */
  public static <R> R findFirstOrNull(Collection<R> collection, Predicate<R> predicate) {
    return findFirst(collection, predicate).orElse(null);
  }

  /**
   * Gibt das erste Element zurueck, auf das die Bedingung zutrifft.
   *
   * @param <R>        Typ des Elements
   * @param collection zu durchsuchende Collection
   * @param predicate  Filterfunktion
   * @return {@link Optional} mit dem gefundenen Wert oder leer
   */
  public static <R> Optional<R> findFirst(Collection<R> collection, Predicate<R> predicate) {
    return collection.stream().filter(predicate).findFirst();
  }

  /**
   * Filtert eine Menge von Elementen anhand eines Praedikats. Dabei gibt das Praedikat die zu behaltenden Elemente an.
   *
   * @param collection      zu filternde Collection
   * @param acceptPredicate Filterfunction
   * @param <T>             Typ der Eingabeelemente
   * @return Liste mit gefilterten Elementen
   */
  public static <T> List<T> filter(Collection<T> collection, Predicate<T> acceptPredicate) {
    return collection.stream().filter(acceptPredicate).collect(Collectors.toList());
  }

  /**
   * Filtert eine Menge von Schlüssel-Wert-Paaren anhand eines Praedikats. Dabei gibt das Praedikat die zu behaltenden Elemente an.
   *
   * @param map             zu filternde Map
   * @param acceptPredicate Filterfunction
   * @param <K>             Typ der Schlüsselelemente
   * @param <V>             Typ der Wertelemente
   * @return Map mit gefilterten Schlüssel-Wert-Paaren
   */
  public static <K, V> Map<K, V> filter(Map<K, V> map, BiPredicate<K, V> acceptPredicate) {
    List<Map.Entry<K, V>> entries = filter(map.entrySet(), entry -> acceptPredicate.test(entry.getKey(), entry.getValue()));
    return toMap(entries, Map.Entry::getKey, Map.Entry::getValue);
  }

  /**
   * Baut eine {@link Map} mit den übergebenen Schlüsseln auf. Die Werte dazu werden anhand der Umwandlungs-Funktion berechnet.
   *
   * @param keys                 Schlüsselmenge
   * @param valueMappingFunction Umwandlungs-Funktion
   * @param <K>                  Schlüssel-Typ
   * @param <V>                  Wert-Typ
   * @return {@link Map} mit berechneten Werten zu den Schlüsseln
   * @see #toMap(Collection, Function, Function)
   */
  public static <K, V> Map<K, V> toMap(Collection<K> keys, Function<K, V> valueMappingFunction) {
    return toMap(keys, Function.identity(), valueMappingFunction);
  }

  /**
   * Baut eine {@link Map} für die übergebene Basismenge auf. Die Schlüssel und Werte dazu werden anhand der jeweiligen Umwandlungs-Funktionen berechnet.
   *
   * @param collection           Basismenge
   * @param keyMappingFunction   Schlüssel-Berechnungsfunktion
   * @param valueMappingFunction Wert-Berechnungsfunktion
   * @param <T>                  Basis-Typ
   * @param <K>                  Schlüssel-Typ
   * @param <V>                  Wert-Typ
   * @return {@link Map} mit berechneten Schlüsseln und Werten zu der Basismenge
   * @see #toMap(Collection, Function)
   */
  public static <T, K, V> Map<K, V> toMap(Collection<T> collection, Function<T, K> keyMappingFunction, Function<T, V> valueMappingFunction) {
    // nicht via Collectors.toMap, weil das dort benutzte merge keine null-Values erlaubt!
    Map<K, V> result = new HashMap<>();
    collection.forEach(item -> result.put(keyMappingFunction.apply(item), valueMappingFunction.apply(item)));
    return result;
  }

  /**
   * Liefert eine {@link List} der angegebenen Laenge, welche die gelieferten Werte beinhaltet.
   * Der {@link Supplier} wird dabei pro Eintrag einmal aufgerufen, um das Listenelement zu erzeugen.
   *
   * @param supplier Supplier zur Erzeugung eines Listenelements
   * @param count    die Anzahl der Listenelemente
   * @param <T>      der Typ der Listenelemente
   * @return Liste mit mehrfach vom {@link Supplier} gelieferten Werten
   */
  public static <T> List<T> multiplicate(Supplier<T> supplier, int count) {
    List<T> result = new ArrayList<>(count);
    for (int i = 0; i < count; i++) {
      result.add(supplier.get());
    }
    return result;
  }

  /**
   * Gruppiert die aufgrund der Id unterschiedlichen Listenelemente in eine {@link Map} mit den verschiedenen Listenelementen als Key und als Value die Anzahl, wie oft das
   * jeweilige Listenelement in der Liste vorkommmt.
   *
   * @param collection eine Liste
   * @param <T>        der Typ der Listenelemente
   * @return eine {@link Map} mit Gruppierung nach per Id unterschiedlicher Listenelemente
   */
  public static <T> Map<T, Long> countMultiplicities(Collection<T> collection) {
    return collection.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
  }

  /**
   * Gibt die angegebene Liste zurueck oder eine leere Liste, falls die Liste null war.
   *
   * @param list eine Liste
   * @param <T>  der Typ der Listenelemente
   * @return die Liste, falls diese nicht null ist, oder eine leere Liste
   */
  public static <T> List<T> ensureNotNull(List<T> list) {
    return (list != null) ? list : Collections.emptyList();
  }

  /**
   * Reiht Werte aneinander und beachtet dabei die Reihenfolge.
   *
   * @param value  Wert
   * @param values weitere Werte
   * @param <T>    Typ der Elemente
   * @return Menge mit aneinandergereihten Werten
   */
  @SafeVarargs
  public static <T> List<T> join(T value, T... values) {
    List<T> result = new LinkedList<>();
    result.add(value);
    result.addAll(Arrays.asList(values));
    return result;
  }

  /**
   * Flacht eine Menge von Mengen zu einer Menge, welche alle Elemente der Untermengen beinhaltet.
   *
   * @param collection Menge
   * @param <T>        Typ der Elemente
   * @return flache Menge
   */
  public static <T> List<T> flatten(Collection<? extends Collection<T>> collection) {
    return collection.stream().flatMap(Collection::stream).collect(Collectors.toList());
  }

  /**
   * Fuehrt eine Menge von Mengen anhand eines Kriteriums zusammen. Dabei wird die Reihenfolge beibehalten.<br/>
   * Die Zusammenfuehrungskriterien werden mittels {@link Object#hashCode()} verglichen.
   *
   * @param mergeCriterionFunction Funktion, welche zu Werten das Zusammenfuehrungskriterium liefert
   * @param collections            Menge von zusammenzufuehrenden Mengen
   * @param <K>                    Typ des Zusammenfuehrungskriteriums
   * @param <V>                    Typ der Werte
   * @return zusammengefuehrte Menge
   */
  @SafeVarargs
  public static <K, V> Collection<V> merge(Function<V, K> mergeCriterionFunction, Collection<V>... collections) {
    Map<K, V> map = new LinkedHashMap<>();
    for (Collection<V> collection : collections) {
      for (V value : collection) {
        map.put(mergeCriterionFunction.apply(value), value);
      }
    }
    return map.values();
  }

  /**
   * Prüft, ob die gegebenen Sammlungen ein gemeinsames Element enthalten.
   *
   * @param collection Sammlung
   * @param items      Sammlung
   * @param <T>        Typ der Elemente
   * @return {@code true}, falls mindestens ein Element in beiden Collections enthalten ist, andernfalls {@code false}.
   */
  public static <T> boolean containsAny(Collection<T> collection, Collection<T> items) {
    return !Collections.disjoint(collection, items);
  }

  /**
   * Prüft ob die gegebene Collection ein Element enthält welches das gegebenen Prädikat erfüllt.
   *
   * @param collection zu durchsuchende Collection
   * @param predicate  Prädikat welches auf die Elemente angewendet wird
   * @param <T>        Typ der Elemente
   * @return true falls mindestens ein Element der Collection dem Prädikat entspricht, andernfalls false.
   */
  public static <T> boolean contains(Collection<T> collection, Predicate<T> predicate) {
    return collection.stream().anyMatch(predicate);
  }

  /**
   * Prüft ob zwei Sammlungen im mathematischem Sinne eine gleiche Menge sind, also dass jeweils eine Sammlung Teilmenge der anderen ist.
   * Was insbesondere auch bedeutet, dass folgende Sammlungen gleich sind: {@code left = [1,2,3,"Hallo","Hallo",3] right = ["Hallo",1,2,3,2]}
   *
   * @param left  Sammlung
   * @param right Sammlung
   * @param <T>   Typ der Elemente
   * @return true falls beide Sammlungen eine gleiche Menge darstellen, andernfalls false
   */
  public static <T> boolean containsEqualElements(Collection<T> left, Collection<T> right) {
    return left.containsAll(right) && right.containsAll(left);
  }

  /**
   * Bildet die Schnittmenge der gegebenen Elementmengen.
   *
   * @param collections Elementmengen
   * @param <T>         Typ der Elemente
   * @return Schnittmenge der Elemente
   */
  public static <T> Set<T> intersect(Collection<Collection<T>> collections) {
    Set<T> result = new HashSet<>();
    Iterator<Collection<T>> iterator = collections.iterator();
    if (iterator.hasNext()) {
      result.addAll(iterator.next());
      iterator.forEachRemaining(result::retainAll);
    } else {
      // nothing to do
    }
    return result;
  }

  /**
   * Bildet die Vereinigungsmenge der gegebenen Elementmengen.
   *
   * @param collections Elementmengen
   * @param <T>         Typ der Elemente
   * @return Vereinigungsmenge der Elemente
   */
  public static <T> Set<T> union(Collection<Collection<T>> collections) {
    Set<T> result = new HashSet<>();
    collections.forEach(result::addAll);
    return result;
  }

  /**
   * Bildet die Differenz der gegebenen Elementmengen.
   * Die Differenz ist nur auf zwei Mengen definiert, daher wird die Differenz
   * sequenziell auf das Ergebnis des vorherigen Schrittes angewendet.
   * Das Ergebnis ist somit die Differenz zwischen dem ersten Element und der Vereinigung
   * der restlichen Mengen.
   *
   * @param collections Elementmengen
   * @param <T>         Typ der Elemente
   * @return Differenz der Elemente
   */
  public static <T> Set<T> setDifference(Collection<Collection<T>> collections) {
    Set<T> result = new HashSet<>();
    Iterator<Collection<T>> iterator = collections.iterator();
    if (iterator.hasNext()) {
      result.addAll(iterator.next());
      iterator.forEachRemaining(result::removeAll);
    } else {
      // nothing to do
    }
    return result;
  }

  /**
   * Bildet die Symmetrische Differenz der gegebenen Elementmengen.
   * Die Symmetrische Differenz ist nur auf zwei Mengen definiert, daher wird die Differenz
   * sequenziell auf das Ergebnis des vorherigen Schrittes angewendet.
   *
   * @param collections Elementmengen
   * @param <T>         Typ der Elemente
   * @return Differenz der Elemente
   */
  public static <T> Set<T> symmetricDifference(Collection<Collection<T>> collections) {
    Iterator<Collection<T>> iterator = collections.iterator();
    Set<T> result = iterator.hasNext() ? new HashSet<>(iterator.next()) : new HashSet<>();
    while (iterator.hasNext()) {
      Collection<T> nextEntry = iterator.next();
      Set<T> union = union(Arrays.asList(result, nextEntry));
      Set<T> intersection = intersect(Arrays.asList(result, nextEntry));
      result = setDifference(Arrays.asList(union, intersection));
    }
    return result;
  }

  /**
   * Calculates the cartesian product of the given sets.
   * If the given list of sets is empty, an empty tuple will be returned.
   * If at least one of the given sets is empty, an empty list (no tuple) is returned.
   *
   * @param sets The list of sets to build the cartesian product for.
   * @return Returns a set of all tuples of the cartesian product. A tuple contains the elements of the sets in the same order as the given sets.
   */
  public static <T> Set<List<T>> cartesianProduct(List<Set<T>> sets) {
    return Sets.cartesianProduct(sets);
  }

  /**
   * Fügt die gegebenen Elemente zu einem String zusammen. Hierfür wird {@link Object#toString()} genutzt.
   *
   * @param collection Sammlung
   * @param separator  Trennzeichen
   * @return zusammengeführter String
   * @see CollectionUtil#toString(Collection, String, Function)
   */
  public static String toString(Collection<?> collection, String separator) {
    return toString(collection, separator, Object::toString);
  }

  /**
   * Fügt die gegebenen Elemente zu einem String zusammen. Hierfür wird die übergebene Funktion genutzt.
   *
   * @param collection       Sammlung
   * @param separator        Trennzeichen
   * @param toStringFunction Funktion, um die String-Repräsentation der einzelnen Elemente zu bilden
   * @return zusammengeführter String
   * @see CollectionUtil#toString(Collection, String)
   * @see CommonUtil#joinOn(String, Collection)
   */
  public static <T> String toString(Collection<T> collection, String separator, Function<T, String> toStringFunction) {
    return CommonUtil.joinOn(separator, convert(collection, toStringFunction));
  }

  /**
   * Wendet den gegebenen Consumer auf alle Elemente der gegebenen Collection an.
   * Der prozentuale Fortschritt wird gelogt.
   *
   * @param <T>                 Typ der Elemente
   * @param source              Die Klasse in der diese Methode aufgerufen wird
   * @param progressDescription Ein Nachricht, die beim loggen ausgegeben werden soll
   * @param collection          Die Menge deren Elemente bearbeitet werden sollen
   * @param consumer            Der anzuwendende Consumer
   */
  public static <T> void processWithLog(Object source, String progressDescription, Collection<T> collection, Consumer<T> consumer) {
    int currentProgress = 0;
    long startTime = new Date().getTime();
    long lastIteration = startTime;
    for (T element : collection) {
      consumer.accept(element);
      currentProgress++;
      lastIteration = logProgress(source, progressDescription, currentProgress, collection.size(), lastIteration, startTime);
    }
  }

  private static long logProgress(Object source, String progressDescription, int currentProgress, int totalProgress, long lastIteration, long startTime) {
    long result;
    int onePercent = totalProgress / 100;
    if ((onePercent > 0) && (currentProgress % onePercent == 0)) {
      int percentage = CommonUtil.convertToInt((double) 100 * currentProgress / totalProgress);
      result = new Date().getTime();
      LogUtil.info(source, progressDescription + " [Progress: {0}% ({1}/{2})]", percentage, currentProgress, totalProgress);
      LogUtil.info(source, progressDescription + " [Time for last iteration: {0}s]", (result - lastIteration) / 1000.0);
      LogUtil.info(source, progressDescription + " [Estimated time until finished: {0}s]", (result - startTime) / 1000.0 / percentage * (100 - percentage));
    } else {
      result = lastIteration;
    }
    return result;
  }
}