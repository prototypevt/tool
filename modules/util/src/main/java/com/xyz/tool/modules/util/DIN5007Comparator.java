package com.xyz.tool.modules.util;

import java.util.Comparator;

public class DIN5007Comparator implements Comparator<String> {

  @Override
  public int compare(String o1, String o2) {
    return normalizeDIN5007(o1).compareTo(normalizeDIN5007(o2));
  }

  private static String normalizeDIN5007(String string) {
    return string.toLowerCase().replace('ä', 'a').replace('ö', 'o').replace('ü', 'u').replace("ß", "ss");
  }
}