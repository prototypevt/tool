package com.xyz.tool.modules.util;

public enum OperatingSystem {
  LINUX {
    @Override
    public final void handle(OperatingSystemHandler handler) {
      handler.linux();
    }
  },
  WINDOWS {
    @Override
    public final void handle(OperatingSystemHandler handler) {
      handler.windows();
    }
  };

  public abstract void handle(OperatingSystemHandler handler);

  public interface OperatingSystemHandler {

    void linux();

    void windows();
  }
}
