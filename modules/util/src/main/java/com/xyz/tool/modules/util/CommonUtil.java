package com.xyz.tool.modules.util;

import org.apache.commons.lang3.SystemUtils;

import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Utility class providing methods for converting or checking of strings, numbers and dates, and other convenience methods.
 */
public abstract class CommonUtil {

  public static final String LINE_SEPARATOR = System.getProperty("line.separator");

  private CommonUtil() {
    // nothing to do
  }

  /**
   * Url encodes the given value using UTF-8 encoding.
   *
   * @param value a text value
   * @return the url encoded value
   */
  public static String getUrlEncodedValue(String value) {
    try {
      return URLEncoder.encode(value, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw CommonUtil.createException(e, "Fehler");
    }
  }

  /**
   * Decodes an url encoded value using UTF-8 encoding.
   *
   * @param value a url encoded text value
   * @return the decoded value
   */
  public static String getUrlDecodedValue(String value) {
    try {
      return URLDecoder.decode(value, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      throw CommonUtil.createException(e, "Fehler");
    }
  }

  /**
   * Base64-encodes a given string value.
   *
   * @param value string value
   * @return Base64-encoded string value
   */
  public static String encodeBase64(String value) {
    return Base64.getEncoder().encodeToString(value.getBytes());
  }

  /**
   * Formats a string from the given pattern and substitution values.
   *
   * @param value      a {@link MessageFormat} pattern (required not to be null)
   * @param parameters the substitution values for the given pattern
   * @return the formatted string
   */
  public static String formatString(String value, String... parameters) {
    return MessageFormat.format(value, (Object[]) parameters);
  }

  /**
   * Formats the given value as decimal string.
   *
   * @param value       The number to format.
   * @param preDecimals The minimum number of positions before the decimal point (at least 1)
   * @param decimals    The number of positions after the decimal point.
   * @param locale      The locale to use.
   * @return The formatted number string.
   */
  public static String formatDecimal(double value, int preDecimals, int decimals, Locale locale) {
    DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(locale);
    format.setRoundingMode(RoundingMode.HALF_UP);
    StringBuilder pattern = new StringBuilder("0");
    for (int i = 1; i < preDecimals; i++) {
      pattern.append("0");
    }
    if (decimals > 0) {
      pattern.append(".");
      for (int i = 0; i < decimals; i++) {
        pattern.append("0");
      }
    } else {
      // do not add decimal point
    }
    format.applyPattern(pattern.toString());
    return format.format(value);
  }

  /**
   * Checks whether the given string is empty.
   * A string is considered empty when it is null or does not contain any non space characters.
   *
   * @param text the string
   * @return whether the string is empty or not
   */
  public static boolean isEmpty(String text) {
    return (text == null) || (text.trim().isEmpty());
  }

  /**
   * Checks whether the given string is not empty.
   *
   * @param text the string
   * @return whether the string is not empty
   * @see #isEmpty(String)
   */
  public static boolean isNotEmpty(String text) {
    return !isEmpty(text);
  }

  /**
   * Compares two strings using the {@link DIN5007Comparator}.
   *
   * @param string1 the first string to be compared
   * @param string2 the second string to be compared
   * @return {@link java.util.Comparator#compare(Object, Object)}
   * @see DIN5007Comparator
   */
  public static int compare(String string1, String string2) {
    return new DIN5007Comparator().compare(string1, string2);
  }

  /**
   * Joins the given strings using the given delimiter.
   * Null values in the elements will be added as the string 'null'.
   * If the given element-list is empty, an empty string is returned.
   *
   * @param delimiter a string to separate the given strings in the returned result
   * @param elements  a list of strings
   * @return the concatenated strings of the elements separated the by delimiter
   * @see CollectionUtil#toString(Collection, String)
   * @see CollectionUtil#toString(Collection, String, Function)
   */
  public static String joinOn(String delimiter, Collection<String> elements) {
    StringJoiner result = new StringJoiner(delimiter);
    elements.forEach(result::add);
    return result.toString();
  }

  /**
   * Converts the given string into a long value.
   * If the given string can not be parsed into a long value, null will be returned.
   *
   * @param value a string containing a number value
   * @return string converted to Long, or null
   */
  public static Long convertToLong(String value) {
    Long result;
    if (isEmpty(value)) {
      result = null;
    } else {
      try {
        result = Long.valueOf(value);
      } catch (NumberFormatException e) {
        result = null;
      }
    }
    return result;
  }

  /**
   * Creates and returns a UUID.
   *
   * @return a UUID
   */
  public static String buildRandomId() {
    return UUID.randomUUID().toString();
  }

  /**
   * Returns a {@link RuntimeException} object initialized with the provided data.
   *
   * @param cause       cause of the exeption
   * @param message     a {@link MessageFormat} pattern (required not null)
   * @param a_parameter the substitution values for the message
   * @return a {@link RuntimeException} object
   */
  public static RuntimeException createException(Exception cause, String message, Object... a_parameter) {
    return new RuntimeException(MessageFormat.format(message, a_parameter), cause);
  }

  /**
   * Converts a date string of form <code>yyyy-MM-dd</code> to a date string <code>dd.MM.yyyy</code>.
   *
   * @param value a date string
   * @return the converted date string
   */
  public static String convertDateToDisplayDate(String value) {
    try {
      String result;
      if (isEmpty(value)) {
        result = "";
      } else {
        SimpleDateFormat dateReader = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = dateReader.parse(value);
        result = dateFormat.format(date);
      }
      return result;
    } catch (ParseException e) {
      throw CommonUtil.createException(e, "Fehler");
    }
  }

  /**
   * Converts the given string to an array of its bytes using UTF-8 character encoding.
   *
   * @param value a string (required not to be null)
   * @return the array of the string's bytes
   */
  public static byte[] convertToByteArray(String value) {
    return value.getBytes(StandardCharsets.UTF_8);
  }

  /**
   * Converts the given bytes to a string using UTF-8 character encoding.
   *
   * @param value an array of bytes
   * @return the string created from the given bytes
   */
  public static String convertToString(byte[] value) {
    return new String(value, StandardCharsets.UTF_8);
  }

  /**
   * Checks whether the string value consists of digits only.
   *
   * @param value a string
   * @return whether the string consists of digits
   */
  public static boolean isDigit(String value) {
    return isNotEmpty(value) && value.matches("\\d*");
  }

  /**
   * Converts the given string into a {@link Double} value.
   * If the given string cannot be parsed into a {@link Double}, the defaultValue will be returned.
   *
   * @param value        a string containing a number value
   * @param defaultValue in case the provided value is empty or an error occurs
   * @return string converted to {@link Double}, or the defaultValue
   */
  public static Double convertToDouble(String value, Double defaultValue) {
    Double result;
    if (CommonUtil.isEmpty(value)) {
      result = defaultValue;
    } else {
      try {
        String adjustedValue = value.trim();
        result = Double.valueOf(adjustedValue);
      } catch (NumberFormatException e) {
        LogUtil.warning(CommonUtil.class, "String cannot be converted to double: {0}", value);
        result = defaultValue;
      }
    }
    return result;
  }

  /**
   * Converts the given string into a double value.
   *
   * @param value a string containing a number value
   * @return string converted to double, or 0.0 in case of an error
   * @see #convertToDouble(String, Double)
   */
  public static double convertToDouble(String value) {
    return convertToDouble(value, 0.0);
  }

  /**
   * Converts the given string into an {@link Integer} value.
   * If the given string cannot be parsed into an {@link Integer}, the defaultValue will be returned.
   *
   * @param value        a string containing a number value
   * @param defaultValue in case the provided value is empty or an error occurs
   * @return string converted to {@link Integer}, or the defaultValue
   */
  public static Integer convertToInt(String value, Integer defaultValue) {
    Integer result;
    if (CommonUtil.isEmpty(value)) {
      result = defaultValue;
    } else {
      try {
        result = Integer.parseInt(value.trim());
      } catch (NumberFormatException e) {
        LogUtil.warning(CommonUtil.class, "String cannot be converted to integer: {0}", value);
        result = defaultValue;
      }
    }
    return result;
  }

  /**
   * Returns <code>true</code> if the given value is greater than zero, and <code>false</code> otherwise.
   *
   * @param value an int
   * @return whether the value is greater than zero
   */
  public static boolean convertToBoolean(int value) {
    return value > 0;
  }

  /**
   * Returns 1 if the given value is <code>true</code>, and 0 otherwise.
   *
   * @param value a boolean
   * @return 1 if value is <code>true</code>, 0 otherwise
   */
  public static int convertToInt(boolean value) {
    return value ? 1 : 0;
  }

  /**
   * Converts the given {@link Double} to its integer value.
   *
   * @param value a {@link Double} (required not to be null)
   * @return the integer value of the given {@link Double}
   */
  public static int convertToInt(Double value) {
    return value.intValue();
  }

  /**
   * Converts the given date to a string of form <code>dd.MM.yyyy</code>.
   * In case of a null input, an empty string is returned.
   *
   * @param date a {@link Date} object
   * @return a date string of form <code>dd.MM.yyyy</code>
   */
  public static String formatDate(Date date) {
    String result;
    if (date != null) {
      result = new SimpleDateFormat("dd.MM.yyyy").format(date);
    } else {
      result = "";
    }
    return result;
  }

  /**
   * Converts the given date to a string of form <code>dd.MM.yyyy hh:mm</code> using the default locale.
   * In case of a null input date, an empty string is returned.
   *
   * @param date a {@link Date} object
   * @return a date string of form <code>dd.MM.yyyy hh:mm</code>
   */
  public static String formatDateTime(Date date) {
    String result;
    if (date != null) {
      result = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(date);
    } else {
      result = "";
    }
    return result;
  }

  /**
   * Returns an object holding information about the operating system the application is running on.
   *
   * @return the operation system information
   */
  public static OperatingSystem getOperatingSystem() {
    OperatingSystem result;
    if (SystemUtils.IS_OS_WINDOWS) {
      result = OperatingSystem.WINDOWS;
    } else if (SystemUtils.IS_OS_LINUX) {
      result = OperatingSystem.LINUX;
    } else {
      throw createException(null, "unexpected operating system: {0}", SystemUtils.OS_NAME);
    }
    return result;
  }

  /**
   * Wraps a {@link Consumer} into a {@link Function} always returning {@code null}.
   *
   * @param consumer Consumer
   * @param <T>      Type of the parameter
   * @param <R>      Type of the result
   * @return Function which delegates to Consumer and always returns {@code null}.
   */
  public static <T, R> Function<T, R> createConsumerFunction(Consumer<T> consumer) {
    return value -> {
      consumer.accept(value);
      return null;
    };
  }
}