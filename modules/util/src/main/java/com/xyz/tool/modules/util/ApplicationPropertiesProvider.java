package com.xyz.tool.modules.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:util.properties")
public class ApplicationPropertiesProvider implements ApplicationContextAware {

  private static final String DEVELOPMENT_MODE_PROPERTY = "application.developmentmode";
  private static boolean developmentMode;

  @Override
  public void setApplicationContext(ApplicationContext context) throws BeansException {
    init(context);
  }

  private void init(ApplicationContext context) {
    Environment environment = context.getEnvironment();
    developmentMode = Boolean.valueOf(environment.getProperty(DEVELOPMENT_MODE_PROPERTY));
  }

  public static boolean isDevelopmentMode() {
    return developmentMode;
  }
}
