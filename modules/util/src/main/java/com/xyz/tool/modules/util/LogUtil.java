package com.xyz.tool.modules.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public abstract class LogUtil {

  private LogUtil() {
  }

  public static void info(Object source, String message, Object... args) {
    obtainLogger(source).info(formatMessage(message, args));
  }

  public static void warning(Object source, String message, Object... args) {
    obtainLogger(source).warn(formatMessage(message, args));
  }

  public static void error(Object source, Throwable cause, String message, Object... args) {
    obtainLogger(source).error(formatMessage(message, args), cause);
  }

  public static void debug(Object source, String message, Object... args) {
    obtainLogger(source).debug(formatMessage(message, args));
  }

  public static void productionWarning(Object source, String message, Object... args) {
    if (ApplicationPropertiesProvider.isDevelopmentMode()) {
      throw new IllegalStateException(formatMessage(message, args));
    } else {
      warning(source, message, args);
    }
  }

  public static void productionError(Object source, Throwable cause, String message, Object... args) {
    if (ApplicationPropertiesProvider.isDevelopmentMode()) {
      throw new IllegalStateException(formatMessage(message, args), cause);
    } else {
      error(source, cause, message, args);
    }
  }

  public static void defaultValue(Object source, String valueName, Object defaultValue) {
    String message = "Default-Wert Nutzung: \"{0}\" wird auf den Standardwert \"{1}\" gesetzt";
    productionWarning(source, message, valueName, defaultValue);
  }

  private static Logger obtainLogger(Object source) {
    Class loggerClass;
    if (source instanceof Class) {
      loggerClass = (Class) source;
    } else {
      loggerClass = source.getClass();
    }
    return LoggerFactory.getLogger(loggerClass);
  }

  private static String formatMessage(String message, Object... args) {
    return MessageFormat.format(message, args);
  }
}