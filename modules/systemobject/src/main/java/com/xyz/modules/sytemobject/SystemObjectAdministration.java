package com.xyz.modules.sytemobject;

import com.xyz.tool.modules.util.CollectionUtil;
import com.xyz.tool.modules.util.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class SystemObjectAdministration<SO extends SystemObject<SOAP>, SOR extends SystemObjectRepository<SO>, SOAP extends SystemObjectArchiveParameter> {

  @Autowired
  private SOR _repository;

  public SO getBySystemId(SystemId systemId) {
    LogUtil.debug(this, "searching system object with originId {0}", systemId);
    return _repository.getBySystemId(convertSystemId(systemId));
  }

  public Collection<SO> getBySystemIdsUnordered(Collection<SystemId> systemIds) {
    return _repository.getBySystemIds(CollectionUtil.convert(systemIds, this::convertSystemId));
  }

  private String convertSystemId(SystemId systemId) {
    return systemId.getValue();
  }

  public List<SO> getBySystemIdsOrdered(List<SystemId> orderedSystemIds) {
    Collection<SO> unorderedObjects = getBySystemIdsUnordered(orderedSystemIds);
    List<SO> orderedObjects = new LinkedList<>(unorderedObjects);
    orderedObjects.sort(Comparator.comparing(so -> orderedSystemIds.indexOf(so.getSystemId())));
    return orderedObjects;
  }

  public List<SO> findAll() {
    return _repository.findAll();
  }

  public List<SystemId> findAllSystemIds() {
    Collection<SO> elements = _repository.findAllSystemIds();
    return CollectionUtil.convert(elements, SystemObject::getSystemId);
  }

  public <P extends SOAP> SO getOrCreate(P parameter) {
    SO result = getBySystemId(parameter.getSystemId());
    if (result == null) {
      result = create(parameter);
    } else {
      // nothing to do
    }
    return result;
  }

  public <P extends SOAP> SO create(P parameter) {
    SO systemObject = instantiateEntity(parameter);
    LogUtil.debug(this, "creating system object for class {0} with originId {1}", systemObject.getClass().getName(), parameter.getSystemId());
    onPreCreate(systemObject, parameter);
    onPreArchive(systemObject, parameter);
    systemObject.create(parameter);
    onCreate(systemObject);
    onArchive(systemObject);
    _repository.save(systemObject);
    onPostCreate(systemObject);
    onPostArchive(systemObject);
    return systemObject;
  }

  private void update(SOAP parameter, SO systemObject) {
    LogUtil.debug(this, "updating system object for class {0} with originId {1}", systemObject.getClass().getName(), parameter.getSystemId());
    onPreUpdate(systemObject, parameter);
    onPreArchive(systemObject, parameter);
    systemObject.update(parameter);
    onUpdate(systemObject);
    onArchive(systemObject);
    _repository.save(systemObject);
    onPostUpdate(systemObject);
    onPostArchive(systemObject);
  }

  public <P extends SOAP> SO archive(P parameter) {
    SO systemObject = _repository.getBySystemId(convertSystemId(parameter.getSystemId()));
    if (systemObject != null) {
      update(parameter, systemObject);
    } else {
      systemObject = create(parameter);
    }
    return systemObject;
  }

  public SO delete(SystemId systemId) {
    SO systemObject = _repository.getBySystemId(convertSystemId(systemId));
    if (systemObject != null) {
      LogUtil.debug(this, "deleting system object with originId {0}", systemId);
      onPreDelete(systemObject);
      _repository.delete(systemObject);
    } else {
      // nothing to do
    }
    return systemObject;
  }

  public void deleteAll() {
    _repository.deleteAll();
  }

  protected SOR getRepository() {
    return _repository;
  }

  @SuppressWarnings("unchecked")
  private SO instantiateEntity(SOAP parameter) {
    return (SO) parameter.instantiateSystemObject();
  }

  /**
   * Moeglichkeit, um auf das Speichern von Objekten vor dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   * @param parameter    Parameter
   */
  protected void onPreArchive(SO systemObject, SOAP parameter) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Erstellen von Objekten vor dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   * @param parameter    Parameter
   */
  protected void onPreCreate(SO systemObject, SOAP parameter) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Aktualisieren von Objekten vor dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   * @param parameter    Parameter
   */
  protected void onPreUpdate(SO systemObject, SOAP parameter) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Speichern von Objekten nach dessen Wertaenderung, aber vor dessen Persistierung, zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onArchive(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Erstellen von Objekten nach dessen Wertaenderung, aber vor dessen Persistierung, zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onCreate(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Aktualisieren von Objekten nach dessen Wertaenderung, aber vor dessen Persistierung, zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onUpdate(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Speichern von Objekten nach dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onPostArchive(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Erstellen von Objekten nach dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onPostCreate(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um auf das Aktualisieren von Objekten nach dessen Wertaenderung und Persistierung zu reagieren.
   *
   * @param systemObject Objekt
   */
  protected void onPostUpdate(SO systemObject) {
    // nothing to do
  }

  /**
   * Moeglichkeit, um vorab auf das Löschen von Objekten zu reagieren.<br/>
   * <b>Achtung:</b> Wird aus Performancegruenden nicht von {@link #deleteAll()} getriggert!
   *
   * @param systemObject Objekt
   */
  protected void onPreDelete(SO systemObject) {
    // nothing to do
  }
}