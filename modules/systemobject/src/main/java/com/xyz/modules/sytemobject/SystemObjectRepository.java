package com.xyz.modules.sytemobject;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Collection;

public interface SystemObjectRepository<SO extends SystemObject> extends MongoRepository<SO, String> {

  @Query(value = "{ '_systemId' : ?0 }")
  SO getBySystemId(String systemId);

  @Query(value = "{ '_systemId' : {$in: ?0 }}")
  Collection<SO> getBySystemIds(Collection<String> systemIds);

  @Query(value = "{}", fields = "{'_systemId' : 1, '_class': 1}")
  Collection<SO> findAllSystemIds();
}
