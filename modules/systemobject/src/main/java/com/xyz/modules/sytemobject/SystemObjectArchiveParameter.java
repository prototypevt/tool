package com.xyz.modules.sytemobject;

public interface SystemObjectArchiveParameter<SO extends SystemObject> {

  SO instantiateSystemObject();

  SystemId getSystemId();
}
