package com.xyz.modules.sytemobject;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;
import java.util.Objects;

public abstract class SystemObject<SOAP extends SystemObjectArchiveParameter> {

  @Id
  private String _id;
  @Indexed
  private String _systemId;
  private Date _insertDate;
  private Date _updateDate;

  public final void create(SOAP parameter) {
    _systemId = parameter.getSystemId().getValue();
    doCreate(parameter);
    _insertDate = new Date();
    _updateDate = _insertDate;
  }

  public final void update(SOAP parameter) {
    doUpdate(parameter);
    _updateDate = new Date();
  }

  public SystemId getSystemId() {
    return SystemId.valueOf(_systemId);
  }

  protected void doCreate(SOAP parameter) {
    doUpdate(parameter);
  }

  protected abstract void doUpdate(SOAP parameter);

  @Override
  public boolean equals(Object obj) {
    return obj instanceof SystemObject && Objects.equals(getSystemId(), ((SystemObject) obj).getSystemId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(_systemId);
  }

  public boolean matchSystemId(SystemId systemId) {
    return Objects.equals(_systemId, systemId.getValue());
  }
}