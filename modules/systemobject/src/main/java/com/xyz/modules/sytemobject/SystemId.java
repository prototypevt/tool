package com.xyz.modules.sytemobject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SystemId implements Comparable<SystemId> {

  private static Map<String, SystemId> _cache;

  static {
    _cache = new HashMap<>();
  }

  private String _value;

  @SuppressWarnings("unused")
  private SystemId() {
    this("");
  }

  private SystemId(String value) {
    _value = value;
  }

  public String getValue() {
    return _value;
  }

  public synchronized static SystemId valueOf(String value) {
    SystemId result;
    if (_cache.containsKey(value)) {
      result = _cache.get(value);
    } else {
      result = new SystemId(value);
      _cache.put(value, result);
    }
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    return obj instanceof SystemId && _value.equals(((SystemId) obj)._value);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(_value);
  }

  @Override
  public String toString() {
    return _value;
  }

  @Override
  public int compareTo(SystemId systemId) {
    return _value.compareTo(systemId.getValue());
  }
}